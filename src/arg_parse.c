/* Author: Luis Fueris 
 * Last updated: August 31, 2019
 * Comments: this file is the interface which calls argp.h library
 * Code modified from:
 * https://www.gnu.org/software/libc/manual/html_node/Argp.html#Argp
 */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <fcntl.h>
#include <time.h>
#include <argp.h>
#include <string.h>
#include "time_debug.h"
#include "arg_parse.h"

const char *argp_program_version = "Fitzroy v0.1";
const char *argp_program_bug_address = 
                        "https://gitlab.unizar.es/699623/fitz-roy/issues";
static char doc[] = "\nGufxvit [guf-ex-vit] is a tool which monitorizes\n"
                    "guests disk images (raw, qcow2, vmdk , vdi, vpc, vhd)\n"
                    "with the aid of libguestfs and Virustotal API";
/* a description of the arguments we accept */
static char args_doc[] = "DISK PATH API_KEY";
/* the options we understand. */
static struct argp_option options[] = 
{
    {"verbose", 'v', 0, 0, 
        "Produce verbose output", 0 },
    {0}
};

/*
 * [parse_opt]: function which loops input aguments
 * @key: argument option
 * @arg: argument
 * @argp_state: stores machine arguments machine state (loop)
 *
 */
static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
  /* Get the input argument from argp_parse, which we
        know is a pointer to our arguments structure. */
    struct arguments *arguments = state->input;
    switch (key) {
        case 'v':
            arguments->verbose = 1;
            break;
        case ARGP_KEY_ARG:
            if (state->arg_num >= 3) {
                /* too many arguments. */
                argp_usage(state);
            }

            arguments->args[state->arg_num] = arg;
            break;
        case ARGP_KEY_END:
            if (state->arg_num < 3) {
                argp_usage(state);
            }

            break;
        default:
            return ARGP_ERR_UNKNOWN;
    }

    return 0;
}


/* Our argument parser:  
 * @options: command line options
 * @parse_opt: function which parse the options
 * @args_doc: arguments description
 * @doc: project documentation
 *
 */
static struct argp argp = { options, parse_opt, args_doc, doc };


/* [parse_args]: function which is called in fitz-roy main function
 * @argc: number of arguments
 * @argv: array pointer to arguments
 * @arguments: pointer to input arguments
 * @ile: file pointer to write into
 *
 */
int arg_parse(int argc, char *argv[], struct arguments *arguments, 
                                        FILE *file) 
{
    int vm_disk_fd;
    int key_len;
#ifdef DEBUG
    char *str_time;
    time_t exec_time;
#endif
    /* sanitize input parameters, default values */
    arguments->verbose = 0;
    argp_parse(&argp, argc, argv, 0, 0, arguments);
#ifdef DEBUG
    time(&exec_time);
    str_time = malloc(sizeof(char) * MAX_DATE);
    extract_time(exec_time, str_time);
    printf("[%s] (DEBUG) source=arg_parse.c arg[0]=%s arg[1]=%s", str_time, 
                                                        arguments->args[0], 
                                                        arguments->args[1]);
    fprintf(file, "[%s] (DEBUG) source=arg_parse.c arg[0]=%s arg[1]=%s", 
                                                        str_time, 
                                                        arguments->args[0], 
                                                        arguments->args[1]);
    printf(" arg[2]=%s verbose=%s\n",  arguments->args[2], 
                                        arguments->verbose ? "yes" : "no");
    fprintf(file, " arg[2]=%s verbose=%s\n",  arguments->args[2], 
                                        arguments->verbose ? "yes" : "no");
#endif
    vm_disk_fd = open(arguments->args[0], O_RDONLY);                                        
    if (vm_disk_fd < 0) {                                                        
        printf("%s does not exist in host filesystem. Please execute help ", 
                                                        arguments->args[0]);               
        printf("menu:\n");
        printf("%s --help\n", argv[0]);
        exit(EXIT_FAILURE);                                                      
    }   

    if (arguments->args[1][0] != '/') {                                           
        printf("%s does not exist in guest filesystem,", arguments->args[1]);     
        printf(" you must specify a valid root filesystem path which will ");
        printf("be begin with /. Please execute:\n");                      
        printf("%s --help\n", argv[0]);
        exit(EXIT_FAILURE);                                                   
    }         

    key_len = strlen(arguments->args[2]);
    if (key_len != API_KEY_LEN) {
        printf("%s must be 64 characters long. Please execute:\n", 
                                                        arguments->args[2]);
        printf("%s --help\n", argv[0]);
    }

release_ptrs:
    /* release memory of string time and exit if input arguments are wrong */
#ifdef DEBUG
    free(str_time);
#endif
    if (vm_disk_fd < 0 || arguments->args[1][0] != '/' || 
        key_len != API_KEY_LEN) {
        exit(EXIT_FAILURE);
    } else {
        return 0;
    }
}
