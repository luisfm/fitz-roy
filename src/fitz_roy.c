/* Author: Luis Fueris 
 * Last updated: August 31, 2019
 * Comments: file provides the main function which analyzes virtual 
 * machine disk and uses virustotal API. Compile as follows:
 *
 */
#define _XOPEN_SOURCE 500
#define _GNU_SOURCE
#include <sched.h>
#include <ftw.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <ftw.h>
#include <dirent.h> 
#include <pthread.h> 
#include <unistd.h> 
#include <fcntl.h>
#include <guestfs.h>
#include <jansson.h>
#include <VtFile.h>
#include <VtResponse.h>

#include "fitz_roy.h" 
#include "time_debug.h" 
#include "arg_parse.h" 


pthread_mutex_t lock_file;

/*
 * [debug__list_partitions]: list guest filesystem partitions, debug 
 * function.
 * @g: guest filesystem handler
 * @file: file pointer to write into
 *
 */
static int debug__list_partitions(guestfs_h *g, FILE *file)
{
    int i;
    time_t exec_time;
    char *str_time;
    char **partitions = guestfs_list_partitions(g);
    if (partitions == NULL) {
        exit(EXIT_FAILURE);
    }

    i = 0;
    exec_time = time(NULL);
    str_time = malloc(sizeof(char) * MAX_DATE);
    extract_time(exec_time, str_time);
    printf("[%s] (DEBUG) source=fitz_roy.c guest partitions:\n", str_time);
    fprintf(file, "[%s] (DEBUG) source=fitz_roy.c guest partitions:\n", 
                                                                str_time);
    while (partitions[i] != NULL) {
        exec_time = time(NULL);
        extract_time(exec_time, str_time);
        printf("[%s] (DEBUG) source=fitz_roy.c \t%s\n", str_time, 
                                                        partitions[i]);
        fprintf(file, "[%s] (DEBUG) source=fitz_roy.c \t%s\n", 
                                                        str_time, 
                                                        partitions[i]);
        if (partitions[i] != NULL) {
            free(partitions[i]);
        }
        
        i++;
    }

    free(str_time);
    free(partitions);
    return 0;
}

/*
 * [debug__list_filesystems]: list guest filesystems, debug function.
 * @g: guest filesystem handler
 * @file: file pointer to write into
 *
 */
static int debug__list_filesystems(guestfs_h *g, FILE *file)
{
    int i;
    time_t exec_time;
    char *str_time;
    char **fsyss = guestfs_list_filesystems(g);
    if (fsyss == NULL) { 
        exit(EXIT_FAILURE);
    }
    
    i = 0;
    exec_time = time(NULL);
    str_time = malloc(sizeof(char) * MAX_DATE);
    extract_time(exec_time, str_time);
    printf("[%s] (DEBUG) source=fitz_roy.c partitions:filesystems\n", 
                                                                str_time);
    fprintf(file, "[%s] (DEBUG) source=fitz_roy.c ", str_time);
    fprintf(file, "partitions:filesystems\n");
                                            
    while (fsyss[i] != NULL) {
        exec_time = time(NULL);
        extract_time(exec_time, str_time);

        printf("[%s] (DEBUG) source=fitz_roy.c \t%s:", str_time, fsyss[i]);
        printf("%s\n", fsyss[i + 1]);
        fprintf(file, "[%s] (DEBUG) source=fitz_roy.c \t%s:", str_time, 
                                                                fsyss[i]);
        fprintf(file, "%s\n", fsyss[i + 1]);
        if (fsyss[i] != NULL && fsyss[i + 1] != NULL) {
            free(fsyss[i]);
            free(fsyss[i + 1]);
        }
        
        i += 2;
    }

    free(str_time);
    free(fsyss);
    return 0;
}

/*
 * [mount_rootfs]: mount the guest partitions on local filesystem.
 * @g: guest filesystem handler
 * @file: file pointer to write into
 *
 */
static int mount_rootfs(guestfs_h *g, FILE *file) 
{
#ifdef DEBUG 
    time_t exec_time;
#endif
    int err;
    int i;
    char *str_time;
    char **partitions = guestfs_list_partitions(g);
    if (partitions == NULL) {
        exit(EXIT_FAILURE);
    }

    i = 0;
    while (partitions[i] != NULL) {
#ifdef DEBUG 
        exec_time = time(NULL);
        str_time = malloc(sizeof(char) * MAX_DATE);
        extract_time(exec_time, str_time);
        printf("[%s] (DEBUG) source=fitz_roy.c", str_time);
        printf("\t%s\n", partitions[i]);
        fprintf(file, "[%s] (DEBUG) source=fitz_roy.c", str_time);
        fprintf(file, "\t%s\n", partitions[i]);
#endif
        err = guestfs_mount(g, partitions[i], "/");
        if (err == 0) {
#ifdef DEBUG
            exec_time = time(NULL); 
            extract_time(exec_time, str_time);
            printf("[%s] (DEBUG) source=fitz_roy.c exit partitions loop\n", 
                                                                str_time);
            fprintf(file, "[%s] (DEBUG) source=fitz_roy.c exit partitions",
                                                                str_time);
            fprintf(file, "loop\n");
#endif
            break;
        }

        if (partitions[i] != NULL) {
            free(partitions);
        }

        i++;
    }

    free(str_time);
    free(partitions);
    return 0;
}

/*
 * [parse_report]: parse json object in order to create a log.
 * @response_str: pointer to string virustotal report
 * @id: thread id
 * @abs_path: file absolute path 
 * @vm_disk: guest image name
 * @file: file pointer to write into
 *
 */
static void parse_vtreport(struct VtResponse *response, pthread_t id, 
                                                        char *abs_path,
                                                        char *vm_disk,
                                                        FILE *file)
{
    time_t exec_time;
    char *scans;
    char *str_time;
    json_t *jdata, *jdata_av, *jdata_detect = NULL;
    int i, detect;
    str_time = malloc(sizeof(char) * MAX_DATE);
    /* extract result from virustotal antivirus, 
       recursive search in json object  */
    jdata = json_object_get(VtResponse_getJanssonObj(response), SCANS);
    for (i = 0; i < N_AVS; i++) { 
        jdata_av = json_object_get(jdata, VT_AVS[i]);
        /* extract detect json value */
        scans = json_dumps(jdata_av, JSON_INDENT(4));
        jdata_detect = json_object_get(jdata_av, DETECTED);
        if (jdata_detect != NULL) {

            pthread_mutex_lock(&lock_file);
            exec_time = time(NULL);
            extract_time(exec_time, str_time);
            detect = json_boolean_value(jdata_detect) == 1 ? 1 : 0;
            printf("[%s] thread=%lu av=%s file=%s guest=%s detect=", 
                                                    str_time, id, VT_AVS[i], 
                                                    abs_path, vm_disk);
            detect == 1 ? printf("true\n") : printf("false\n");
            fprintf(file, "[%s] thread=%lu av=%s file=%s guest=%s detect=", 
                                                    str_time, id, VT_AVS[i], 
                                                    abs_path, vm_disk);
            detect == 1 ? fprintf(file, "true\n") : fprintf(file, "false\n");
            pthread_mutex_unlock(&lock_file);

        }
    }

    pthread_mutex_lock(&lock_file);
    printf("\n");
    pthread_mutex_unlock(&lock_file);

    free(str_time);
    if (jdata != NULL) {
        free(jdata);
    }
}

/*
 * [get_vtreply]: a thread which wait to virus total reply in order to 
 * detect illegitimate binaries.
 * @args: th_args struct which has the following fields:
 *      @file_scan: pointer to file that we need to analyze
 *      @abs_path: absolute file path
 *      @vm_disk: pointer to guest image
 *      @file: file pointer to write into
 *
 */
static void *get_vtreply(void *_args)
{
    int ret, response_code;
    struct VtResponse *response;
    char *response_str, *scan_id_str = NULL;
    time_t exec_time = time(NULL);
    char *str_time;
    struct th_args *args = (struct th_args *) _args;
    const pthread_t id = pthread_self();
    cpu_set_t cpuset;
    /* set cpu affinity */
    CPU_ZERO(&cpuset);
    CPU_SET(args->core, &cpuset);
    ret = pthread_setaffinity_np(id, sizeof(cpu_set_t), &cpuset);
    if ( ret < 0) {
        printf("Error when createing cpu affinity in thread=%lu\n", id);
        pthread_exit((void *) EXIT_FAILURE);
    }

    pthread_mutex_lock(&lock_file);
    str_time = malloc(sizeof(char) * MAX_DATE);
    extract_time(exec_time, str_time);
    printf("[%s] thread=%lu file=%s guest=%s\n", str_time, id, 
                                                args->abs_path, 
                                                args->vm_disk);
    fprintf(args->file, "[%s] thread=%lu file=%s guest=%s\n", str_time, id, 
                                                            args->abs_path, 
                                                            args->vm_disk);
    pthread_mutex_unlock(&lock_file);

    response = VtFile_getResponse(args->file_scan);
    if (response != NULL) {
        scan_id_str = VtResponse_getString(response, SCAN_ID);

        pthread_mutex_lock(&lock_file);
        exec_time = time(NULL);
        extract_time(exec_time, str_time);
        printf("[%s] thread=%lu scan_id=%s file=%s guest=%s\n", 
                                            str_time, id, scan_id_str, 
                                            args->abs_path, args->vm_disk);
        fprintf(args->file, "[%s] thread=%lu scan_id=%s file=%s guest=%s\n", 
                                            str_time, id, scan_id_str, 
                                            args->abs_path, args->vm_disk);
        pthread_mutex_unlock(&lock_file);

        sleep(WAIT_TIME + 5);
        ret = VtFile_report(args->file_scan, scan_id_str);
        if (ret == 0) {
            response = VtFile_getResponse(args->file_scan);
            VtResponse_getResponseCode(response, &response_code);
            while (response_code == RESOURCE_QUEUED) {
#if DEBUG
                pthread_mutex_lock(&lock_file);
                exec_time = time(NULL);
                extract_time(exec_time, str_time);
                printf("[%s] (DEBUG) source=fitz_roy.c thread=%lu ", 
                                                            str_time, id);
                printf("scan_id=%s state=queued file=%s guest=%s\n", 
                                                scan_id_str, args->abs_path, 
                                                args->vm_disk);
                fprintf(args->file, "[%s] (DEBUG) source=fitz_roy.c ", 
                                                                str_time); 
                fprintf(args->file, "thread=%lu ", id);
                fprintf(args->file, "scan_id=%s state=queued file=%s ", 
                                                scan_id_str, 
                                                args->abs_path);
                fprintf(args->file, "guest=%s\n", args->vm_disk);
                pthread_mutex_unlock(&lock_file);
#endif
                sleep(WAIT_TIME + 5);
                ret = VtFile_report(args->file_scan, scan_id_str);
                response = VtFile_getResponse(args->file_scan);
                VtResponse_getResponseCode(response, &response_code);
            }

            response_str = VtResponse_toJSONstr(response, 
                                                    VT_JSON_FLAG_INDENT);
            if (response_str != NULL) {
                parse_vtreport(response, id, args->abs_path, args->vm_disk, 
                                                                args->file);
            }
        } else {
            pthread_mutex_lock(&lock_file);
            extract_time(exec_time, str_time);
            if (ret == ERR_RATE_LIMIT) {
                printf("[%s] thread=%lu scan_id=%s state=request_rate_limit ", 
                                                str_time, id, scan_id_str); 
                printf("file=%s, guest=%s\n", args->abs_path, args->vm_disk);
            } else if (ret == ERR_BAD_REQUEST) {
                printf("[%s] thread=%lu scan_id=%s state=bad_request", 
                                                str_time, id, scan_id_str); 
                printf("file=%s, guest=%s\n", args->abs_path, args->vm_disk);
            } else if (ret == ERR_FORBIDDEN) {
                printf("[%s] thread=%lu scan_id=%s state=forbidden", 
                                                str_time, id, scan_id_str); 
                printf("file=%s, guest=%s\n", args->abs_path, args->vm_disk);
            }
            pthread_mutex_unlock(&lock_file);
            
            goto release_ptrs;
        }
    }

release_ptrs:
    /* release pointers memory of thread function */
    free(_args);
    free(str_time);
    if (response_str != NULL) {
        free(response_str);
    }

    if (scan_id_str != NULL) {
        free(scan_id_str);
    }

    if (ret == ERR_RATE_LIMIT || ret == ERR_BAD_REQUEST || 
        ret == ERR_FORBIDDEN) {
        pthread_exit((void *) EXIT_FAILURE);
    } else {
        pthread_exit(NULL);
    }
}

/*
 * [analyze_binaries]: analyze guest filesystem binaries (or files) that
 * directory specified by user contains.
 * @api_key: string api key pointer 
 * @vm_disk: pointer to guest image
 *
 */
static int analyze_binaries(const char *api_key, char *vm_disk, FILE *file) 
{
    int ret, i, file_len, abs_path_len, vm_disk_len;
    int _host_dir_len = HOST_DIR_LEN;
    struct VtFile *file_scan;
    time_t exec_time = time(NULL);
    DIR *dir_ptr;
    struct dirent *directory;
    char *abs_path = NULL;
    int n_requests = N_REQUESTS;
    char *str_time;
    int n_ths = 0;
    /* cpu binding  max. 2 threads if you 
       want to use public virustotal api */
    pthread_t th_reply[N_THREADS];
    /* init cpu affinity and lock of session file */
    ret = pthread_mutex_init(&lock_file, NULL); 
    if (ret < 0) {
        printf("Lock file cannot be initialized\n");
        exit(EXIT_FAILURE);
    }

    vm_disk_len = strlen(vm_disk);
    file_scan = VtFile_new();
    VtFile_setApiKey(file_scan, api_key);
    dir_ptr = opendir(HOST_DIR);
    if (dir_ptr != NULL) {
        str_time = malloc(sizeof(char) * MAX_DATE);
        directory = readdir(dir_ptr);
        while (directory != NULL) {
            if (!(strncmp(directory->d_name, 
                            FATHER_DIR, FATHER_DIR_LEN) == 0 ||
                strncmp(directory->d_name, 
                            CURRENT_DIR, CURRENT_DIR_LEN) == 0)) {
                file_len = strlen(directory->d_name);
                abs_path_len = _host_dir_len + file_len;
                abs_path = malloc(sizeof(char) * (abs_path_len + 1));
                strncpy(abs_path, HOST_DIR, _host_dir_len);
                strncpy(&abs_path[_host_dir_len], directory->d_name, 
                                                            file_len + 1);
                
                exec_time = time(NULL);
                extract_time(exec_time, str_time);
                printf("[%s] we are going to scan %s file of %s\n", 
                                                        str_time, abs_path, 
                                                        vm_disk);
                fprintf(file, "[%s] we are going to scan %s file of %s\n", 
                                                        str_time, abs_path, 
                                                        vm_disk);
                exec_time = time(NULL);
                ret = VtFile_scan(file_scan, abs_path, NULL);
                if (ret == 0) {
                    struct th_args *args = malloc(sizeof(struct th_args));
                    args->core = n_ths;
                    args->file = file;
                    args->file_scan = file_scan; 
                    args->abs_path = malloc(sizeof(char) * 
                                                    (abs_path_len + 1));
                    args->vm_disk = malloc(sizeof(char) * 
                                                    (vm_disk_len + 1));
                    strncpy(args->abs_path, abs_path, abs_path_len + 1);
                    strncpy(args->vm_disk, vm_disk, vm_disk_len + 1);
                    pthread_create(&th_reply[n_ths], NULL, &get_vtreply, 
                                                                    args);
                    n_ths++;
                    if (n_ths >= N_THREADS) {
                        for (n_ths = 0; n_ths < N_THREADS; n_ths++) {
                            pthread_join(th_reply[n_ths], NULL);
                        }
                        n_ths = 0;
                        sleep(WAIT_TIME);
                    }
                } else {
                    extract_time(exec_time, str_time);
                    printf("[%s] Virustotal response error %d\n", 
                                                            str_time, ret);
                    fprintf(file, "[%s] Virustotal response error %d\n", 
                                                            str_time, ret);
                }

                free(abs_path);
            }
            /* one request is made by main 
                process and another one by thread */
            //sleep(WAIT_TIME);
            directory = readdir(dir_ptr);
        }

        free(str_time);
        closedir(dir_ptr);
    }
    /* special case, one file in directory */
    if (n_ths == 1) {
        pthread_join(th_reply[0], NULL);
    }
 
    pthread_mutex_destroy(&lock_file);
    return 0;
}

/*
 * [rm_prev_files]: remove previous files of @HOST_DIR directory if it 
 * exists.
 * @path: absolute file path
 * @sb: data pointer returned by stat() function
 * @typeflag: file tree walkflags
 * @ftwbuf: file tree walk struct pointer
 * 
 */
static int rm_prev_files(const char *path, const struct stat *sb, 
                                    int typeflag, struct FTW *ftwbuf)
{
    int result = remove(path);
    if (result != 0) {
        perror(path);
    }

    return result;
}

/*
 * [extract_binaries]: extract guest binarioes (or files) that are in the 
 * directory specified by the user.
 * @g: guest filesystem handler
 * @path_to_search: pointer to second argument which is the absolute path
 * @file: file pointer to write into
 *
 */
static int extract_binaries(guestfs_h *g, const char *path_to_search, 
                                            FILE* file)
{
    int i, status, path_to_search_len, file_len, abs_path_len;
    struct stat local_dir_stat;
    char *abs_path = NULL;
    time_t exec_time = time(NULL);
    char *str_time;
    char **files = guestfs_find(g, path_to_search);
    if (files == NULL) { 
        printf("The path %s doest not exist, please ", path_to_search);
        printf(" specify a valid guest filesystem path\n");
        exit(EXIT_FAILURE);
    }

    if (stat(HOST_DIR, &local_dir_stat) == 0) {
        /* remove suspicious-files dir if exists */
        nftw(HOST_DIR, rm_prev_files, 64, FTW_DEPTH);
    } 

    status = mkdir(HOST_DIR, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if (status < 0 ) {
        printf("The directory %s can not be created\n", HOST_DIR);
        goto release_ptrs;
    }

    str_time = malloc(sizeof(char) * MAX_DATE);
    extract_time(exec_time, str_time);
    printf("[%s] directories and files starting at %s:\n\n", str_time, 
                                                            path_to_search);
    fprintf(file, "[%s] directories and files starting at %s:\n\n", str_time, 
                                                            path_to_search);
    i = 0;
    path_to_search_len = strlen(path_to_search);
    while (files[i] != NULL) {
        file_len = strlen(files[i]);
        abs_path_len = path_to_search_len + file_len;
        abs_path = malloc(sizeof(char) * (abs_path_len + 1));

        strncpy(abs_path, path_to_search, path_to_search_len);
        strncpy(&abs_path[path_to_search_len], files[i], file_len + 1);
        printf("\b\b\b\b- %s\n", abs_path);
        fprintf(file, "\b\b\b\b- %s\n", abs_path);
        guestfs_copy_out(g, abs_path, HOST_DIR);

        if (files[i] != NULL) {
            free(files[i]);
        }
        free(abs_path);

        i++;
    }

    printf("\n");
    fprintf(file, "\n");
    /* the caller must free the strings and array after use */
    free(str_time);

release_ptrs:
    /* release pointer of guest files */
    free(files);
    if (status < 0) {
        exit(EXIT_FAILURE);
    } else {
        return 0;
    }
}

/*
 * [main]: main function.
 * @argc: number of arguments
 * @argv: array pointer to arguments
 *
 */
int main(int argc, char *argv[]) 
{
    int err, i;
    int redir_err_null;
    guestfs_h *g;
    struct arguments *arguments;
    char *vm_disk, *path_to_search, *api_key, *str_time = NULL;
    /* +4 for .log characters */
    char filename[FITZ_ROY_LEN + MAX_DATE + 5];
    time_t exec_date = time(NULL);
    time_t exec_time; 
    struct tm *tm_date;
    FILE *file;
    /* create session log file */
    tm_date = gmtime(&exec_date);
    strftime(filename, FITZ_ROY_LEN + MAX_DATE + 5, 
                        "/var/log/fitzroy-%Y%m%d%H%M.log", tm_date);
    file = fopen(filename, "w");
    if (file < 0) {
        printf("File %s could not be opened\n", filename);
        goto close_file;
    }
    /* begin input parameters sanitize, call to parse module */
    arguments = malloc(sizeof(struct arguments));
    arg_parse(argc, argv, arguments, file);
    g = guestfs_create();
    if (g == NULL) {
        printf("Libguestfs could not be created\n");
        exit(EXIT_FAILURE);
    }

    vm_disk = arguments->args[0];
    path_to_search = arguments->args[1];
    api_key = arguments->args[2];
    /* end input input parametes sanitize */
    guestfs_add_drive(g, vm_disk);
    err = guestfs_launch(g);
    if (err != 0) {
        printf("Libguestfs could not be launched\n");
        goto release_ptrs;
    }

#ifdef DEBUG 
    debug__list_partitions(g, file);
    debug__list_filesystems(g, file);
#endif
    mount_rootfs(g, file);
    /* redirect stderr to null, virustatal api generates 
        error messages that we need to control */
    redir_err_null = open("/dev/null", O_RDWR);
    dup2(redir_err_null, STDERR_FILENO);

    extract_binaries(g, path_to_search, file);
    analyze_binaries(api_key, vm_disk, file);

release_ptrs:
    /* release pointers memory and close guest filesystem */
    guestfs_shutdown(g);
    guestfs_close(g);
    if (arguments != NULL) {
        free(arguments);
    }

    if (file != NULL) {
        fclose(file);
    }

close_file:
    /* close file if it fails or program ends */
    exec_time = time(NULL);
    str_time = malloc(sizeof(char) * MAX_DATE);
    extract_time(exec_time, str_time);
    printf("[%s] Session log has been created %s\n", str_time, filename);
    if (err != 0 || file != 0) {
        free(str_time);
        exit(EXIT_FAILURE);
    }

    return 0;
}
