/* Author: Luis Fueris
 * Last updated: August 25, 2019
 * Comments: fitz_roy header file
 *
 */

#ifndef FITZ_ROY_H
#define FITZ_ROY_H

#define HOST_DIR_LEN        19
#define FATHER_DIR_LEN      2
#define CURRENT_DIR_LEN     1
#define N_REQUESTS          4
#define RESOURCE_QUEUED     -2
#define ERR_RATE_LIMIT      204
#define ERR_BAD_REQUEST     400
#define ERR_FORBIDDEN       403
#define N_AVS               52
#define MAX_DATE            20
#define FITZ_ROY_LEN        16

struct th_args {                                                                 
    struct VtFile *file_scan;                                                    
    char *abs_path;                                                              
    char *vm_disk;                                                              
    FILE *file;                                                              
    int core;
};                                                                               
                                                                                 
const char *HOST_DIR = "./suspicious-files/";
const char *SCAN_ID = "scan_id";
const char *RESPONSE_CODE = "response_code";
const char *SCANS = "scans";
const char *DETECTED = "detected";
const char *FATHER_DIR = "..";
const char *CURRENT_DIR = ".";
const char *VT_AVS[] = { "Bkav", "MicroWorld-eScan", "FireEye", 
                        "CAT-QuickHeal", "McAfee", "Malwarebytes", 
                        "Zillya", "K7AntiVirus", "K7GW",
                        "Baidu", "Cyren", "Symantec", "ESET-NOD32",
                        "TrendMicro-HouseCall", "ClamAV", "Kaspersky",
                        "BitDefender", "NANO-Antivirus", "ViRobo",
                        "SUPERAntiSpyware", "Tencent", "Ad-Aware",
                        "Emsisoft", "Comodo", "F-Secure", 
                        "DrWeb", "VIPRE", "TrendMicro",
                        "McAfee-GW-Edition", "CMC", "Sophos",
                        "F-Prot", "Jiangmin", "Avira",
                        "Antiy-AVL", "Kingsoft", "Arcabit",
                        "Aegislab", "AhnLab-V3", "ZoneAlarm",
                        "Avast-Mobile", "Microsoft", "TACHYON",
                        "TotalDefense", "VBA32", "ALYac",
                        "MAX", "Zoner", "Rising",
                        "Ikarus", "GData", "Panda",
                        "Qihoo-360" };

#endif 
