/* Author: Luis Fueris
 * Last updated: September 18, 2019
 * Comments: arg_parse header file
 *
 */

#ifndef ARG_PARSE_H
#define ARG_PARSE_H

#define API_KEY_LEN         64
#define MAX_DATE            20

struct arguments {
    char *args[2];
    int verbose;
    char *output_file;
};

int arg_parse(int argc, char *argv[], struct arguments *arguments, 
                                        FILE *file);

#endif 
