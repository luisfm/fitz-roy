/* Author: Luis Fueris
 * Last updated: July 04, 2019
 * Comments: time_debug header file
 *
 */

#ifndef TIME_DEBUG_H
#define TIME_DEBUG_H

#define SIZE_MATCH_TOK      8
#define MAX_DATE            20
void extract_time(const time_t time, char *str_time);

#endif 
