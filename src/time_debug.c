/* Author: Luis Fueris
 * Last updated: August 31, 2019
 * Comments: split the time and return <hh>:<mm>:<ss>
 */
#include <time.h>
#include <string.h>
#include <stdio.h>
#include "time_debug.h"


/* 
 * [extract_time]: splits the time return by asctime function
 * @time: seconds from epoch time
 * @str_time: pointer to reserverd date
 *
 */
void extract_time(const time_t time, char *str_time)
{
    struct tm *tm = localtime(&time);
    strftime(str_time, MAX_DATE, "%y/%m/%d - %I:%M %p", tm);
}
