#!/bin/bash

#set -x

QEMU=$(which qemu-system-x86_64) 

#$QEMU \
#-hda ../disk-images/deb-clean.img \
#-m 512 \
#-k en \
#-net nic -net user,hostfwd=tcp::2222-:22 &
##-net nic,macaddr=52:54:00:12:69:69 -net tap,ifname=tap0

$QEMU \
-hda ../disk-images/deb-infected.img \
-m 512 \
-k en \
-net nic -net user,hostfwd=tcp::4444-:22 &

exit 0
