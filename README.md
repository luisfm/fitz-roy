# Fitz Roy: a free solo climbing to sanitize virtual machines

## Preface

Nowadays when a computer security incident occurs, it is necessary to 
quarantine  the machine (physicical or virtualized) in order to avoid data 
leaks. Current malware has multiples phases that exchange information with 
C&C (*Command an Crontrol*) servers, therefore it needs to be mitigated as 
soon as possible. A possible solution could be to monitor entry points of 
nodes for instance application directory (``/var/www/<site>/html``). 
*Fitz Roy* monitorizes Linux guests filesystems (*raw*, *qcow2*, 
*vmdk* , *vdi*, *vpc*, *vhd*) relying on 
[libguestfs](http://libguestfs.org) and 
[Virustotal's API](https://www.virustotal.com/gui/home/upload). 
[Libguestfs](http://libguestfs.org) mounts virtual machine filesystem and 
uploads suspicious files to 
[Virustotal's API](https://www.virustotal.com/gui/home/upload) which are then
analyzed and a malware detection report is generated. The project has 
been implemented in C and built with 
[``autotools``](https://www.gnu.org/software/automake/manual/html_node/Autotools-Introduction.html).
The tool can be fitted under DFIR (*Digital Forensic and Incident Response*)
field because it can be useful in *Eradication* and 
*Recovery* phases. It also can be used in *Preparation* phases in order to
check virtual machine backups.

## Index

1. [Introduction](#introduction)
2. [Setup](#setup)
3. [Install](#install)
4. [Usage](#usage)
4. [Bugs](#bugs)

## Introduction <a name="introduction"></a>

[Libguestfs](http://libguestfs.org) provides terrific tools 
([virt-df](http://libguestfs.org/virt-df.1.html), 
[virt-builder](http://libguestfs.org/virt-df.1.html), 
[virt-v2v](http://libguestfs.org/virt-v2v.1.html),
[virt-rescue](http://libguestfs.org/virt-rescue.1.html),
[virt-win-reg](http://libguestfs.org/virt-win-reg.1.html)) and libraries
([C and C++](http://libguestfs.org/guestfs.3.html)) to delve into 
guest machines. Several tools may be useful from DFIR point of view. For 
instance, [virt-win-reg](http://libguestfs.org/virt-win-reg.1.html) tool can
copy Windows Registry entries into local machine without powering up the 
guest. [Virustotal](https://www.virustotal.com/gui/home/upload) is a great 
meta-antivirus analyzer which provides a entry point for automatic triage, it 
can also accept IP addresses and domain names as input. The output report 
has a graph with the communications it establishes (if it is known malware), 
providing a great insight. VirusTotal team implements a fantastic 
[C API](https://github.com/VirusTotal/c-vtapi) with the aim of automating 
malware analysis.

*Fitz Roy* tool provides a few use cases, for example:
when a machine (whether virtualized or not) is infected by a malicious 
eavesdropper, it has to be quarantined and we need to deploy a clean guest
backup. That procedure should be like this in order to stop the malware 
infection. Before that we could analyze the guest filesystem in a safe way 
with *Fitz Roy* even without powering it up. It can also be used to make 
a guest backup introspection and mark it as clean (or infected). At some 
point in the future, application logs could be integrated with 
[ElasticSearch](https://www.elastic.co/products/elasticsearch) in order 
to generate a dynamic filesystem monitorization of virtual machines. These 
points are the main project goals. Here are some advantages, but there is a
disadvantage: [Virustotal API](https://www.virustotal.com/gui/home/upload) 
limit request (and the delay that this entails):

- *4 requests/minute*
- *5760 requests/day*
- *172800 requests/month*

That handicap could be overcome by requesting a 
[premium API key](https://www.virustotal.com/gui/contact-us/premium-services).

![General scheme](diagrams/fitz-roy-diagram.png "General scheme")

## Setup <a name="setup"></a> 

First we need to setup the development libraries. ``Fitzroy`` tool has
the following dependencies:

- [``libguestfs``](http://libguestfs.org) should be downloaded by repository 
or by compiling source code. Now we can install by repository (*easy mode*). 
The useful packages are the following:

```bash
$ yum install libguestfs-devel 
```
It could also be interesting to download command line tools if we need to
copy any file to our safety environment: ``libguestfs`` makes accesses in 
order to modify virtual machine disk images and ``libguestfs-winsupport`` 
enables support for Windows guests to *virt-v2v* and *virt-p2v*

```bash
$ yum install libguestfs libguestfs-winsupport
```

- [``jansson``](http://www.digip.org/jansson/) library executes functions 
with JSON format. [Virustotal](https://www.virustotal.com/gui/home/upload) 
library uses it so we are going to install via repository.

```bash
$ yum install jansson-devel jansson
```

- [``c-vtapi``](https://github.com/VirusTotal/c-vtapi) must be
downloaded in order to make requests to VirusTotal API. So please donwload 
via ``git clone`` and follow these steps.

```bash
$ git clone https://github.com/VirusTotal/c-vtapi
$ cd c-vtapi
$ autoreconf -fi
$ ./configure
$ make
$ make install
```

> Note: ``libcvtapi.so`` shared library is installed in ```/usr/local/lib```,
so please export ``LD_LIBRARY_PATH`` properly.

## Install <a name="install"></a> 

Now we are going to compile ``fitzroy`` tool with the following commands.

```bash
$ git clone git@gitlab.unizar.es:699623/fitz-roy.git
$ cd fitz-roy/src/
$ autoreconf -fi
$ automake --add-missing
$ ./configure
$ make
```

If you want to enable debug mode which includes verbose output and ``gdb`` 
symbols, please execute ``configure`` script with this option.

```bash
./configure --enable-debug
```

If you want to install ``fitzroy`` tool in your ``rootfs``, please
execute the following command with ``sudo``.

```bash
$ sudo make install
```

Or if you want to install it in curren directory, execute as:

```bash
$ make
```

It is important to note that you can control number of threads and time
between request. In order to do that, please export environment
variables as follows:

```bash
$ export FITZROY_THREADS=4
$ export FITZROY_TIME=60
$ ./configure
$ make
```

Default values for using public API key are 
``FITZROY_THREADS=2`` ``FITZROY_TIME=30``. 


## Usage <a name="usage"></a> 

After we have compiled ``fitzroy`` binary as above, now we are going to see 
some execution examples. Please note that API key below is a placeholder,
replace it with your [own API key](https://www.virustotal.com/gui/join-us).

> Note: please, execute ``fitzroy`` with ``root`` privileges because it 
creates a log file as follows ``/var/log/fitzroy-<YYYYMMDDhhmm>.log``.

```bash
$ ./fitzroy ../vms/deb-infected.img /etc/init.d/ cea89529e50249419e4ff680f62d896c6116f112d8b2164197ce16f2d31bb6bd
[19/08/31 - 03:35 PM] directories and files starting at /etc/init.d/:

- /etc/init.d/console-setup.sh
- /etc/init.d/cron
- /etc/init.d/dbus
- /etc/init.d/hwclock.sh
- /etc/init.d/keyboard-setup.sh
- /etc/init.d/kmod
- /etc/init.d/kns813.ico
- /etc/init.d/networking
- /etc/init.d/procps
- /etc/init.d/rsyslog
- /etc/init.d/ssh
- /etc/init.d/udev

[19/08/31 - 03:35 PM] we are going to scan ./suspicious-files/cron file of ../vms/deb-infected.img
[19/08/31 - 03:35 PM] we are going to scan ./suspicious-files/console-setup.sh file of ../vms/deb-infected.img
[19/08/31 - 03:35 PM] thread=139921918498560 file=./suspicious-files/cron guest=../vms/deb-infected.img
[19/08/31 - 03:35 PM] thread=139921918498560 scan_id=c84c64b776f027f84357617ee3995b920dc7112014bcda3722bed3dcf332c26e-1567257812 file=./suspicious-files/cron guest=../vms/deb-infected.img
[19/08/31 - 03:35 PM] thread=139921903699712 file=./suspicious-files/console-setup.sh guest=../vms/deb-infected.img
[19/08/31 - 03:35 PM] thread=139921903699712 scan_id=a4fecda40d06d41cab9892b8c2832d3f41d333d944a91a9bc7334540d1cada26-1567257812 file=./suspicious-files/console-setup.sh guest=../vms/deb-infected.img
[19/08/31 - 03:36 PM] thread=139921918498560 av=Bkav file=./suspicious-files/cron guest=../vms/deb-infected.img detect=false
[19/08/31 - 03:36 PM] thread=139921918498560 av=MicroWorld-eScan file=./suspicious-files/cron guest=../vms/deb-infected.img detect=false
[19/08/31 - 03:36 PM] thread=139921918498560 av=FireEye file=./suspicious-files/cron guest=../vms/deb-infected.img detect=false
[19/08/31 - 03:36 PM] thread=139921918498560 av=CAT-QuickHeal file=./suspicious-files/cron guest=../vms/deb-infected.img detect=false
[19/08/31 - 03:36 PM] thread=139921918498560 av=McAfee file=./suspicious-files/cron guest=../vms/deb-infected.img detect=false
[19/08/31 - 03:36 PM] thread=139921918498560 av=Malwarebytes file=./suspicious-files/cron guest=../vms/deb-infected.img detect=false
 
...
```

If you want to enable *debug* mode, please execute ``configure`` script
with ``--enable-debug`` flag. The output will be as follows:

> Note: you can also enable more debugging via exporting following enviroment
variables: 
``LIBGUESTFS_DEBUG=1`` and ``LIBGUESTFS_TRACE=1``. These variables debug ``libguestfs`` components, for instance 
[``supermin``](https://github.com/libguestfs/supermin).

```bash
$ ./fitzroy ../vms/deb-infected.img /etc/init.d/ cea89529e50249419e4ff680f62d896c6116f112d8b2164197ce16f2d31bb6bd
[19/08/31 - 03:46 PM] (DEBUG) source=arg_parse.c arg[0]=../vms/deb-infected.img arg[1]=/etc/init.d/ arg[2]=cea89529e50249419e4ff680f62d896c6116f112d8b2164197ce16f2d31bb6bd verbose=yes
[19/08/31 - 03:46 PM] (DEBUG) source=fitz_roy.c guest partitions:
[19/08/31 - 03:46 PM] (DEBUG) source=fitz_roy.c     /dev/sda1
[19/08/31 - 03:46 PM] (DEBUG) source=fitz_roy.c     /dev/sda2
[19/08/31 - 03:46 PM] (DEBUG) source=fitz_roy.c     /dev/sda5
[19/08/31 - 03:46 PM] (DEBUG) source=fitz_roy.c partitions:filesystems
[19/08/31 - 03:46 PM] (DEBUG) source=fitz_roy.c     /dev/sda1:ext4
[19/08/31 - 03:46 PM] (DEBUG) source=fitz_roy.c     /dev/sda2:unknown
[19/08/31 - 03:46 PM] (DEBUG) source=fitz_roy.c     /dev/sda5:swap
[19/08/31 - 03:46 PM] (DEBUG) source=fitz_roy.c /dev/sda1
[19/08/31 - 03:46 PM] (DEBUG) source=fitz_roy.c exit partitions loop
[19/08/31 - 03:46 PM] directories and files starting at /etc/init.d/:

- /etc/init.d/console-setup.sh
- /etc/init.d/cron
- /etc/init.d/dbus
- /etc/init.d/hwclock.sh
- /etc/init.d/keyboard-setup.sh
- /etc/init.d/kmod
- /etc/init.d/kns813.ico
- /etc/init.d/networking
- /etc/init.d/procps
- /etc/init.d/rsyslog
- /etc/init.d/ssh
- /etc/init.d/udev

[19/08/31 - 03:46 PM] we are going to scan ./suspicious-files/cron file of ../vms/deb-infected.img
[19/08/31 - 03:46 PM] we are going to scan ./suspicious-files/console-setup.sh file of ../vms/deb-infected.img
[19/08/31 - 03:46 PM] thread=139981914457856 file=./suspicious-files/cron guest=../vms/deb-infected.img
[19/08/31 - 03:46 PM] thread=139981914457856 scan_id=c84c64b776f027f84357617ee3995b920dc7112014bcda3722bed3dcf332c26e-1567259201 file=./suspicious-files/cron guest=../vms/deb-infected.img
[19/08/31 - 03:46 PM] thread=139981899659008 file=./suspicious-files/console-setup.sh guest=../vms/deb-infected.img
[19/08/31 - 03:46 PM] thread=139981899659008 scan_id=a4fecda40d06d41cab9892b8c2832d3f41d333d944a91a9bc7334540d1cada26-1567259202 file=./suspicious-files/console-setup.sh guest=../vms/deb-infected.img
[19/08/31 - 03:47 PM] (DEBUG) source=fitz_roy.c thread=139981914457856 scan_id=c84c64b776f027f84357617ee3995b920dc7112014bcda3722bed3dcf332c26e-1567259201 state=queued file=./suspicious-files/cron guest=../vms/deb-infected.img
[19/08/31 - 03:47 PM] thread=139981914457856 av=Bkav file=./suspicious-files/cron guest=../vms/deb-infected.img detect=false
[19/08/31 - 03:47 PM] thread=139981914457856 av=MicroWorld-eScan file=./suspicious-files/cron guest=../vms/deb-infected.img detect=false
[19/08/31 - 03:47 PM] thread=139981914457856 av=FireEye file=./suspicious-files/cron guest=../vms/deb-infected.img detect=false
[19/08/31 - 03:47 PM] thread=139981914457856 av=CAT-QuickHeal file=./suspicious-files/cron guest=../vms/deb-infected.img detect=false
[19/08/31 - 03:47 PM] thread=139981914457856 av=McAfee file=./suspicious-files/cron guest=../vms/deb-infected.img detect=false
[19/08/31 - 03:47 PM] thread=139981914457856 av=Malwarebytes file=./suspicious-files/cron guest=../vms/deb-infected.img detect=false 
...
```

You can contact me via email *luisfueris at gmail dot es* or twitter 
[lfm3773](https://twitter.com/lfm3773). Any feedback is welcome :v: 

## Bugs <a name="bugs"></a> 

According to 
[libguestfs FAQ](http://libguestfs.org/guestfs-faq.1.html) there is a bug when
running ``libguestfs`` as *root*, please check the following response and
workaround:

> Note: You get a permission denied error when opening a disk image, even 
though you are running libguestfs as root. This is caused by ``libvirt``, and 
so only happens when using the ``libvirt`` backend. When run as *root*, 
``libvirt`` decides to run the qemu appliance as user *qemu.qemu*. 
Unfortunately this usually means that qemu cannot open disk 
images, especially if those disk images are owned by *root*, or are present 
in directories which require *root* access. There is a bug open against 
``libvirt`` to fix this: 
[bugzilla.redhat.com/show_bug.cgi?id=1045069](https://bugzilla.redhat.com/show_bug.cgi?id=1045069).

You can work around this by one of the following methods:

1. Switch to the direct backend. ``LIBGUESTFS_BACKEND`` environment variable
specifies the backend which ``libguestfs`` package uses. If you want what
backend is using, please execute the following commands:

```bash
$ unset LIBGUESTFS_BACKEND
$ guestfish get-backend
```
So if you are using the libvirt backend, you can try without 
(ie. libguestfs directly launching qemu) by doing:

```bash
 export LIBGUESTFS_BACKEND=direct
```

Or you can use ``libvirt`` virtualization API library:

```bash
$ export LIBGUESTFS_BACKEND=libvirt
```
```bash
$ export LIBGUESTFS_BACKEND=libvirt:qemu:///session
```

2. Don’t run libguestfs as *root*.

3. Chmod the disk image and any parent directories so that the *qemu* user 
can access them.

4. Edit ``/etc/libvirt/qemu.conf`` and change the user setting (a piggy 
solution).
